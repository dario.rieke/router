<?php 
namespace DarioRieke\Router;

/**
 * RouteInterface
 */
interface RouteInterface {

	/**
	 * get callback to run if route is matched
	 * 
	 * not required to be a valid callable, as it could be resolved
	 * later by a callable resolver
	 * @return string|array|callable
	 */
	public function getCallback();

	/**
	 * get the valid HTTP methods
	 * @return array
	 */
	public function getMethods(): array;

	/**
	 * get the expression with placeholders for regexes 
	 *
	 * example: "/test/{parameter}"
	 * @return string
	 */
	public function getExpression(): string;

	/**
	 * get an array of patterns indexed by placeholder name
	 *
	 * example: [ 'parameter' => '.+' ]
	 * 
	 * @return iterable
	 */
	public function getPatterns(): iterable;
}