<?php 
namespace DarioRieke\Router;

use DarioRieke\Router\RouteInterface;

/**
 * RouteCollectionInterface
 */
class Route implements RouteInterface {

	/**
	 * @var string|array|callable
	 */
	private $callback;

	/**
	 * @var string
	 */
	private $methods;

	/**
	 * @var string
	 */
	private $expression;

	/**
	 * key maps to placeholder in expression, value is the regex to validate against
	 * @var array
	 */
	private $patterns;

	/**
	 * create a new Route object
	 * @param string         			$expression 	path with optional placeholders
	 *                                       			surrounded by { } (e.g. /calendar/{year})
	 * @param string|array|callable   	$callback   	callback to evaluate when route 
	 *                                              	is matched. Not required to be a 
	 *                                               	valid callable
	 * @param array    		 			$patterns   	associative array of name => pattern to 
	 *                                    				match the expressions placeholders 
	 * @param string   		 			$methods     	HTTP methods
	 * @return void
	 */
	public function __construct(string $expression, $callback, array $patterns = [], array $methods = ['GET']) {
		$this->callback = $callback;
		$this->methods = $methods;
		$this->expression = $expression;
		$this->patterns = $patterns;
	}

	public function getCallback() {
		return $this->callback;
	}

	public function getMethods(): array {
		return $this->methods;
	}

	public function getExpression(): string {
		return $this->expression;
	}

	public function getPatterns(): iterable {
		return $this->patterns;
	}
}