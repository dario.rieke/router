<?php 
namespace DarioRieke\Router;

use DarioRieke\Router\RouteCollectionInterface;
use DarioRieke\Router\RouteInterface;

/**
 * RouteCollection
 */
class RouteCollection implements RouteCollectionInterface {

	/**
	 * array of Routes
	 * @var array RouteInterface[]
	 */
	private $routes;

	/**
	 * construct new RouteCollection
	 */
	public function __construct() {

	}

	public function getRoutes(): iterable {
		return $this->routes;
	}

	public function add(RouteInterface $route) {
		$this->routes[] = $route;
	}
}