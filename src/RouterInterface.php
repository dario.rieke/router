<?php 
namespace DarioRieke\Router;

use DarioRieke\Router\RoutingResultInterface;

/**
 * RouterInterface
 */
interface RouterInterface {

	/**
	 * match a Request with a route and return a controller function
	 * 
	 * @throws DarioRieke\Router\Exception\NotFoundException 
	 *         if the Route was not found
	 *         
	 * @throws DarioRieke\Router\Exception\MethodNotAllowedException 
	 *         if wrong method was supplied
	 *         
	 * @param string
	 *        $requestUrl
	 *        the requested url
	 *        
	 * @param string
	 *        $httpMethod 
	 *        the requested http method 
	 *        
	 * @return RoutingResultInterface
	 *         the result of the routing
	 */
	public function match(string $requestUrl, string $httpMethod): RoutingResultInterface;
}

 ?>
