<?php 
namespace DarioRieke\Router;

/**
 * RoutingResultInterface
 */
interface RoutingResultInterface {

	/**
	 * get callback to run if route is matched
	 * 
	 * not required to be a valid callable, as it could be resolved
	 * later by a callable resolver
	 * @see  DarioRieke\Router\RouteInterface
	 * @return string|array|callable
	 */
	public function getCallback();

	/**
	 * setter for callback
	 * @param string|array|callable $callback 
	 */
	public function setCallback($callback): void;

	/**
	 * setter for arguments
	 * @param array $arguments 
	 */
	public function setArguments(array $arguments): void;


	/**
	 * get all resolved route arguments
	 * 
	 * example: [ 'id' => 232, 'param' => 'myString'] 
	 * 
	 * @return array array of arguments
	 */
	public function getArguments(): array;

}