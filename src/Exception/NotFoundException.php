<?php 
namespace DarioRieke\Router\Exception;

use DarioRieke\Router\Exception\RouterExceptionInterface;

/**
 * NotFound Exception
 */
class NotFoundException extends \RuntimeException implements RouterExceptionInterface {

}