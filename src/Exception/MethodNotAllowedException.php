<?php 
namespace DarioRieke\Router\Exception;

use DarioRieke\Router\Exception\RouterExceptionInterface;

/**
 * MethodNotAllowed Exception
 */
class MethodNotAllowedException extends \RuntimeException implements RouterExceptionInterface {

}