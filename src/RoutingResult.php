<?php 
namespace DarioRieke\Router;

use DarioRieke\Router\RoutingResultInterface;

/**
 * RoutingResult
 */
class RoutingResult implements RoutingResultInterface {

	/**
	 * callback
	 * @var string|array|callable
	 */
	private $callback;

	/**
	 * arguments
	 * @var array
	 */
	private $arguments = [];

	public function getCallback() {
		return $this->callback;
	}

	public function setCallback($callback): void {
		$this->callback = $callback;
	}

	public function getArguments(): array {
		return $this->arguments;
	}

	public function setArguments(array $arguments): void {
		$this->arguments = $arguments;
	}

}