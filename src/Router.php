<?php 
namespace DarioRieke\Router;

use DarioRieke\Router\Exception\MethodNotAllowedException;
use DarioRieke\Router\Exception\NotFoundException;
use DarioRieke\Router\RouteCollectionInterface;
use DarioRieke\Router\RoutingResultInterface;
use DarioRieke\Router\RoutingResult;


/**
 * Router 
 */
class Router implements RouterInterface {
	/**
	 * route definitions
	 * @var RouteCollectionInterface
	 */
	private $routeCollection;

	/**
	 * create a new Router
	 * @param RouteCollectionInterface $routeCollection collection of routes to match
	 */
	public function __construct(RouteCollectionInterface $routeCollection) {	
		$this->routeCollection = $routeCollection;
	}

	public function match(string $requestUrl, string $requestHttpMethod): RoutingResultInterface {
		$wrongMethod = false;
		$matchFound = false;

		$routingResult = new RoutingResult();

		foreach ($this->routeCollection->getRoutes() as $route) {

			$expression = $route->getExpression();
			$patterns = $route->getPatterns();
			$methods = $route->getMethods();
			$callback = $route->getCallback();

			//check the route prefix if it exists
			$prefix = explode('{', $expression, 2);
			if(strlen(trim($prefix[0]))) {
				//check if prefix matches before running preg_match for performance reasons
				if(strpos($requestUrl, $prefix[0]) !== 0) {
					continue;
				}
			}

			// check if '{' exists first before running preg_match for performance reasons
			if(strpos($expression, '{') > -1) {
				// extract the parameter names from route expression
				$regex = '/\{([^}]+)\}/';
				if(preg_match_all($regex, $expression, $paramMatches)) {
					$routeParams = $paramMatches[1];

					//replace placeholders with actual regex patterns
					$regex = $expression;

					foreach ($routeParams as $param) {
						//use provided or defaul pattern
						if(array_key_exists($param, $patterns)) {
							$pattern = $patterns[$param];
						}
						else {
							$pattern = '.+';
						}

						//add ( ) capturing group to patterns
						$regex = str_replace('{'.$param.'}', '('.$pattern.')', $regex); 
					}

					$regex = '#^'.$regex.'$#';

					//check if route matches
					if(preg_match($regex, $requestUrl, $matches)) {
						$matchFound = true;
						//remove first match (full path)
						unset($matches[0]);
						//set the matching values
						$finalMatches = array_combine($routeParams, $matches);
						$routingResult->setArguments($finalMatches);

						//if method validates, set callback
						if(\in_array($requestHttpMethod, $methods)) {
							$routingResult->setCallback($callback);
							break;
						}
						else {
							$wrongMethod = true;
						}
					}
				}
			}
			//if no placeholder exists, compare the routes directly
			else {
				$matchFound = ($requestUrl === $expression);
				if($matchFound) {
					if(\in_array($requestHttpMethod, $methods)) {
						$routingResult->setCallback($callback);
						break;
					}
					else {
						$wrongMethod = true;
					}
				}
			}
		}

		$allowed = implode(', ', $methods);
		if($wrongMethod) throw new MethodNotAllowedException("Wrong HTTP Method, given: {$requestHttpMethod}, allowed: {$allowed}");
		if(!$matchFound) throw new NotFoundException("Route {$requestUrl} not found");

		return $routingResult;
		 
	}
}

?>