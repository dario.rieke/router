<?php 
namespace DarioRieke\Router;

use DarioRieke\Router\RouteInterface;

/**
 * RouteCollectionInterface
 */
interface RouteCollectionInterface {

	/**
	 * return the defined routes 
	 * @return array RouteInterface[]
	 */
	public function getRoutes(): iterable;

	/**
	 * add a Route to the Collection
	 * @param RouteInterface $route Route to add
	 */
	public function add(RouteInterface $route);
}