<?php 
declare(strict_types=1);

namespace DarioRieke\Router\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Router\RoutingResult;
use DarioRieke\Router\RoutingResultInterface;

class RoutingResultTest extends TestCase {

	/**
	 * @var Router
	 */
	public $routingResult; 
	
	public function setUp(): void {
		$this->routingResult = new RoutingResult();
	}

	public function testImplementsRoutingResultInterface() {
		$this->assertInstanceOf(RoutingResultInterface::class, $this->routingResult);
	}

	public function testCanSetAndGetCallback() {
		$expected = 'myCallback';
		$this->routingResult->setCallback($expected);
		$this->assertSame($expected, $this->routingResult->getCallback());
	}

	public function testCanSetAndGetArguments() {
		$expected = [
			'test' => 'bar',
			'parameter2' => 'foo'
		];
		$this->routingResult->setArguments($expected);
		$this->assertSame($expected, $this->routingResult->getArguments());
	}
}