<?php 
declare(strict_types=1);

namespace DarioRieke\Router\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Router\RouteCollectionInterface;
use DarioRieke\Router\RouteCollection;
use DarioRieke\Router\RouteInterface;

class RouteCollectionTest extends TestCase {

	/**
	 * @var Router
	 */
	public $routeCollection; 
	
	public function setUp(): void {
		$this->routeCollection = new RouteCollection();
	}

	public function testImplementsRouteCollectionInterface() {
		$this->assertInstanceOf(RouteCollectionInterface::class, $this->routeCollection);
	}

	public function testCanAddAndReturnRoutes() {
		$route1 = $this->getRouteMock();
		$route2 = $this->getRouteMock();
		$route3 = $this->getRouteMock();
		$route4 = $this->getRouteMock();

		$routes = [
			$route1,
			$route2,
			$route3,
			$route4,
		];

		foreach ($routes as $route) {
			$this->routeCollection->add($route);	
		}

		$iterable = $this->routeCollection->getRoutes();
		$this->assertSame($route1, $iterable[0]);
		$this->assertSame($route2, $iterable[1]);
		$this->assertSame($route3, $iterable[2]);
		$this->assertSame($route4, $iterable[3]);
		$this->assertCount(4, $iterable);
	}

	public function getRouteMock() {
		return $this->createMock(RouteInterface::class);
	}
}