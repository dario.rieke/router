<?php 
declare(strict_types=1);

namespace DarioRieke\Router\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Router\RouterInterface;
use DarioRieke\Router\RouteCollectionInterface;
use DarioRieke\Router\Router;
use DarioRieke\Router\RouteInterface;
use DarioRieke\Router\RoutingResultInterface;
use DarioRieke\Router\Exception\MethodNotAllowedException;
use DarioRieke\Router\Exception\NotFoundException;

class RouterTest extends TestCase {

	public function testMatchesTheCorrectRoute() {
		$routeCollectionMock = $this->getRouteCollectionMock();

		$router = new Router($routeCollectionMock);

		$this->assertInstanceOf(RouterInterface::class, $router);

		//create some route mocks
		$callback1 = function() {};
		$route1 = $this->getRouteMock('/route/noParameter/noMatch', $callback1, [], ['GET']);

		$callback2 = 'ControllerString';
		$route2 = $this->getRouteMock('/route/noParameter/noMatchEither', $callback2, [], ['GET']);

		$callback3 = ['custom', 'handler'];
		$route3 = $this->getRouteMock('/route/noParameter/match', $callback3, [], ['GET']);


		$routes = [
			$route1, $route2, $route3
		];

		$routeCollectionMock->method("getRoutes")->willReturn($routes);

		$result = $router->match('/route/noParameter/match', 'GET');

		$this->assertInstanceOf(RoutingResultInterface::class, $result);
		$this->assertSame($route3->getCallback(), $result->getCallback());


		return $router;
	}

	/**
	 * @depends testMatchesTheCorrectRoute
	 */
	public function testMatchesTheCorrectRouteWithParameters() {

		$callback = 'myCallback';
		$route = $this->getRouteMock(
			'/route/{parameter1}/{parameter2}', 
			$callback, 
			[
				//parameter1 should capture anything, parameter2 is restricted to the alphabet
				'parameter2' => '[a-zA-Z]+'
			],
			['GET']
		);

		$routeCollectionMock = $this->getRouteCollectionMock();
		$routeCollectionMock->method("getRoutes")->willReturn([$route]);

		$router = new Router($routeCollectionMock);

		$result = $router->match('/route/thisIsParameter1/thisIsSecondParameter', 'GET');

		$arguments = $result->getArguments();

		$expected = [
			'parameter1' => 'thisIsParameter1',
			'parameter2' => 'thisIsSecondParameter'
		];

		$this->assertSame($expected, $arguments);
	}

	/**
	 * @depends testMatchesTheCorrectRoute
	 */
	public function testThrowsNotFoundExceptionIfRouteNotFound($router) {
		$this->expectException(NotFoundException::class);
		$router->match('thisRouteDoesNotExist!', 'POST');
	}

	/**
	 * @depends testMatchesTheCorrectRoute
	 */
	public function testThrowsMethodNotAllowedExceptionIfWrongMethod($router) {
		$this->expectException(MethodNotAllowedException::class);
		//route matches but method does not
		$router->match('/route/noParameter/match', 'DELETE');
	}

	//test exception throwing 

	public function getRouteCollectionMock() {
		return $this->createMock(RouteCollectionInterface::class);
	}


	public function getRouteMock($path, $callback, $patterns, $methods) {
		$mock = $this->createMock(RouteInterface::class);
		$mock->method('getCallback')->willReturn($callback);
		$mock->method('getMethods')->willReturn($methods);
		$mock->method('getExpression')->willReturn($path);
		$mock->method('getPatterns')->willReturn($patterns);

		return $mock;
	}
}