<?php 
declare(strict_types=1);

namespace DarioRieke\Router\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\Router\RouteInterface;
use DarioRieke\Router\Route;

class RouteTest extends TestCase {

	/**
	 * @var Router
	 */
	public $route; 
	
	public function setUp(): void {
		$this->route = new Route(
			'/path/{parameter}/',
			'callback',
			[ 'parameter' => '[a-z]+' ],
			['GET', 'POST']
		);
	}

	public function testImplementsRouteInterface() {
		$this->assertInstanceOf(RouteInterface::class, $this->route);
	}

	public function testCanReturnExpression() {
		$this->assertSame('/path/{parameter}/', $this->route->getExpression());
	}

	public function testCanReturnCallback() {
		$this->assertSame('callback', $this->route->getCallback());
	}

	public function testCanReturnPatterns() {
		$this->assertSame([ 'parameter' => '[a-z]+' ], $this->route->getPatterns());
	}

	public function testCanReturnMethods() {
		$this->assertSame(['GET', 'POST'],  $this->route->getMethods());
	}
}