# Router

A php router which maps routes with named parameters to callbacks

## Installation

install via composer

```
    "require": {
        "dariorieke/router": "dev-master"
    }
```

## Running tests

run tests with the following command

```
./vendor/bin/phpunit .\tests
```
## Usage

###Basic Usage

Use the router to match paths and run callbacks

```php
use DarioRieke\Router\RouteCollection;
use DarioRieke\Router\Route;
use DarioRieke\Router\Router;
use DarioRieke\Router\Exception\MethodNotAllowedException;
use DarioRieke\Router\Exception\NotFoundException;

//collection of routes
$routeCollection = new RouteCollection();

//add a route to the collection
$routeCollection->add(new Route(
	//url to match
	'/home/',
	//callback to run if route is matched
	function() {
		echo "Hello World!";
	}
));

//add collection to the router
$router = new Router($routeCollection);


try {
	//match a route to a given path
	$result = $router->match('/home/', 'GET');
} 
catch (MethodNotAllowedException | NotFoundException $e) {
	//when no matching route is found a NotFoundException will be thrown
	//when the HTTP method does not match a MethodNotAllowedException will be thrown 
}

//get the callback
$callback = $result->getCallback();
//run it
$callback();

```

### Usage with URL parameters

You can use url parameters with regex 

```php
$routeCollection->add(new Route(
	//url to match with a named parameter
	'/api/user/{username}/',
	//callback to run if route is matched
	function($username) {
		echo "Hello $username!";
	}
));
```

To get the parameters values, use the RoutingResult

```php
$result = $router->match('/api/user/Administrator3000/');

//get all parameters from the url 
$routeParameters = $result->getArguments();

//get the callback
$callback = $result->getCallback();

//run the callback with the parameter from the route
$callback($routeParameters['username']);
```

You can also provide custom regex patterns for your parameters with the third parameter of the `Route`s constructor. If the pattern is not matched by any route a `NotFoundException` will be thrown.
```php
$routeCollection->add(new Route(
	//url to match with a named parameter
	'/api/user/{username}/',
	//callback to run if route is matched
	function($username) {
		echo "Hello $username!";
	},
	[
		//now the route will only match letters of the alphabet
		'username' => '[a-zA-Z]+'
	]
));
```

### Different HTTP methods

The last parameter of the Route Constructor is an array of allowed HTTP methods:

```php
$routeCollection->add(new Route(
	//url to match with a named parameter
	'/api/user/{username}/',
	//callback to run if route is matched
	function($username) {
		echo "Hello $username!";
	},
	[
		//now the route will only match letters of the alphabet
		'username' => '[a-zA-Z]+'
	],
	//only POST and HEAD are allowed
	['POST', 'HEAD']
));
```